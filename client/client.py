import collections
import gflags
import json
import os
import sys
import time
import urllib
import urllib2

FLAGS = gflags.FLAGS

gflags.DEFINE_enum(
  'level', 'basic', ['basic', 'advanced'], 'The level of game to play.')
gflags.DEFINE_enum(
  'solution', 'advanced', ['simple', 'possibility', 'advanced'],
  'The algorithm to use')
gflags.DEFINE_string(
  'username', 'test_user', 'Your username to use.')
gflags.DEFINE_string(
  'password', 'pass', 'Your password to use.')
gflags.DEFINE_integer(
  'total_games', 10, 'Number of games to play.')
gflags.DEFINE_enum(
  'env', 'local', ['local', 'prod'], 'Environment of the server.')

_WORDS_FILE_PATH = os.path.join(
  os.path.dirname(os.path.realpath(__file__)), '../data/words_en.txt')


def _make_request(api_command, params=None):
  """Make a request to Hangman server.

  It will automatically append username and password into the request based on
  flag values.

  Args:
    api_command: A str, the API command to call.
    params: A dict, additional parameters of the API.

  Returns:
    The JSON response.
  """
  if FLAGS.env == 'local':
    hangman_server_url = 'http://localhost:8080'
  else:
    hangman_server_url = 'https://hangman-fbwolf.appspot.com'

  if not params:
    params = {}
  params['username'] = FLAGS.username
  params['password'] = FLAGS.password

  response = urllib2.urlopen('%s/%s?%s' % (
    hangman_server_url, command, urllib.urlencode(params)))
  response_json = json.loads(response.read())
  return response_json

def start_basic_game():
  """Start a basic game.

  Returns:
    The ID of the game just created.
  """
  response_json = _make_request('start_game')
  game_id = response_json['game_id']
  return game_id

def start_advanced_game():
  """Start an advanced game.

  Returns:
    The ID of the game just created.
  """
  response_json = _make_request('start_advanced_game')
  game_id = response_json['game_id']
  return game_id

def get_current_progress(game_id):
  """Get the current progress of a game.

  Args:
    game_id: A str, the game ID.

  Returns:
    The JSON response.
  """
  params = {
    'game_id': game_id,
  }
  response_json = _make_request('get_current_progress', params)
  return response_json

def guess(game_id, letter):
  """Guess a letter.

  Args:
    game_id: A str, the game ID.
    letter: A str, the letter to guess. It should contain only one char.

  Returns:
    The JSON response.
  """
  params = {
    'game_id': game_id,
    'letter': letter,
  }
  response_json = _make_request('guess', params)
  return response_json

class SolutionBase():
  """The base class of solution client."""

  def solve(self, game_id):
    """Solve a Hangman game.

    This is the base function of a solution class. Every subclass should
    implement this function.

    Args:
      game_id: A str, the ID of the Hangman game.

    Returns:
      The failure times after game is finished.
    """
    raise NotImplementedError

class SolutionSimple(SolutionBase):
  """A simple solution class.

  This solution would just guess letters one by one until game is finished.

  Note: It could only solve a basic game.
  """

  def solve(self, game_id):
    for letter in 'abcdefghijklmnopqrstuvwxyz':
      # Guess a letter.
      response_json = guess(game_id, letter)
      print 'Guess: %s, Result: %s, Failures: %s, Finished: %s' % (
        letter, response_json['guess_word'], response_json['failure_times'],
        response_json['finished'])

      # Return if game is finished.
      if response_json['finished']:
        return response_json['failure_times']
    return -1

class SolutionPossibility(SolutionBase):
  """A solution class with usage of possibility.

  This solution would guess the letter with maximum possibility.

  Note: It could only solve a basic game.
  """

  def __init__(self):
    # Load words.
    self._words = []
    f = open(_WORDS_FILE_PATH, 'r')
    for line in f:
      line = line.strip()
      self._words.append(line)
    f.close()

  def _get_letter_with_max_possibility(
    self, game_id, guess_word, possible_words, letter_used):
    count_by_letter = collections.defaultdict(int)
    for word in possible_words:
      for letter in word:
        if letter in letter_used:
          continue

        count_by_letter[letter] += 1

    max_count = 0
    max_letter = 0
    for letter, count in count_by_letter.iteritems():
      if count > max_count:
        max_letter = letter
        max_count = count

    print 'Max Count: %s, Total Count: %s, Letter: %s' % (
      max_count, len(possible_words), max_letter)
    return max_letter

  def _get_possible_words(self, guess_word, words):
    possible_words = []
    for word in words:
      for c1, c2 in zip(guess_word, word):
        if c1 != '_' and c1 != c2:
          break
      else:
        possible_words.append(word)
    return possible_words

  def solve(self, game_id):
    response_json = get_current_progress(game_id)
    guess_word = response_json['guess_word']
    word_len = len(guess_word)

    # Initialize the possible words based on word length.
    possible_words = []
    for word in self._words:
      if len(word) == word_len:
        possible_words.append(word)

    letter_used = set()
    while True:
      # Get the letter with max possibility.
      letter = self._get_letter_with_max_possibility(
        game_id, guess_word, possible_words, letter_used)

      # Guess the letter.
      response_json = guess(game_id, letter)
      print 'Guess: %s, Result: %s, Failures: %s, Finished: %s' % (
        letter, response_json['guess_word'], response_json['failure_times'],
        response_json['finished'])

      # Return if game is finished.
      if response_json['finished']:
        return response_json['failure_times']

      letter_used.add(letter)

      # Get current possible words after the guess.
      guess_word = response_json['guess_word']
      possible_words = self._get_possible_words(guess_word, possible_words)

    return -1


class SolutionAdvanced(SolutionBase):
  _STATIC_LETTERS = ' etaoinshrdlcumwfgypbvkjxqz\'.'

  def __init__(self):
    # Load words.
    self._words = []
    f = open(_WORDS_FILE_PATH, 'r')
    for line in f:
      line = line.strip()
      self._words.append(line)
    f.close()

  def _get_letter_with_max_possibility(
    self, game_id, guess_words, all_possible_words, letter_used):
    max_possibility = 0
    max_letter = None
    for guess_word, possible_words in zip(guess_words, all_possible_words):
      if '_' not in guess_word:
        continue

      count_by_letter = collections.defaultdict(int)
      count_total = 0
      for word in possible_words:
        for letter in word:
          if letter in letter_used:
            continue

          count_by_letter[letter] += 1
          count_total += 1

      if count_total == 0:
        # If count_total == 0, the guess word is not in our dictionary.
        # In this case, we just skip the word and use others to calculate the
        # possibility.
        print 'Word %s is not in our dictionary' % guess_word
      else:
        for letter in SolutionAdvanced._STATIC_LETTERS:
          if letter in letter_used:
            continue
          possibility = float(count_by_letter[letter]) / count_total
          if possibility > max_possibility:
            max_possibility = possibility
            max_letter = letter

    if max_letter is None:
      print 'No words were found in our dictionary!'
      # Only some words that are not in our dictionary left here.
      # In this case, use a letter from static list instead.
      for letter in SolutionAdvanced._STATIC_LETTERS:
        if letter not in letter_used:
          max_letter = letter
          break

    print 'Max Possibility: %s, Letter: %s' % (
      max_possibility, max_letter)
    return max_letter

  def _get_possible_words(self, guess_word, words):
    possible_words = []
    for word in words:
      if len(guess_word) != len(word):
        continue

      for c1, c2 in zip(guess_word, word):
        if c1 != '_' and c1 != c2:
          break
      else:
        possible_words.append(word)
    return possible_words

  def _get_all_possible_words(
    self, guess_words, all_words, original_words=None):
    all_possible_words = []
    if original_words:
      for guess_word in guess_words:
        possible_words = self._get_possible_words(guess_word, original_words)
        all_possible_words.append(possible_words)
    else:
      for guess_word, words in zip(guess_words, all_words):
        possible_words = self._get_possible_words(guess_word, words)
        all_possible_words.append(possible_words)
    return all_possible_words

  def solve(self, game_id):
    response_json = get_current_progress(game_id)
    guess_word = response_json['guess_word']
    word_len = len(guess_word)

    # Initialize
    letter = ' '
    response_json = guess(game_id, letter)
    print 'Guess: %s, Result: %s, Failures: %s, Finished: %s' % (
      letter, response_json['guess_word'], response_json['failure_times'],
      response_json['finished'])
    if response_json['finished']:
      return response_json['failure_times']

    letter_used = set([letter])
    guess_words = response_json['guess_word'].split()
    all_possible_words = self._get_all_possible_words(
      guess_words, None, original_words=self._words)

    while True:
      letter = self._get_letter_with_max_possibility(
        game_id, guess_words, all_possible_words, letter_used)
      response_json = guess(game_id, letter)
      print 'Guess: %s, Result: %s, Failures: %s, Finished: %s' % (
        letter, response_json['guess_word'], response_json['failure_times'],
        response_json['finished'])
      if response_json['finished']:
        return response_json['failure_times']

      guess_words = response_json['guess_word'].split()
      letter_used.add(letter)
      all_possible_words = self._get_all_possible_words(
        guess_words, all_possible_words)

    return -1

def main(argv):
  try:
    argv = FLAGS(argv)  # parse flags
  except gflags.FlagsError as e:
    print('%s\nUsage: %s ARGS\n%s' % (e, sys.argv[0], FLAGS))
    sys.exit(1)

  total_failures = 0
  total_games = FLAGS.total_games
  for i in xrange(total_games):
    if FLAGS.level == 'basic':
      game_id = start_basic_game()
    else:
      game_id = start_advanced_game()
    response_json = get_current_progress(game_id)
    print 'Game #%s: %s' % (i+1, response_json['guess_word'])

    if FLAGS.solution == 'simple':
      solution = SolutionSimple()
    elif FLAGS.solution == 'possibility':
      solution = SolutionPossibility()
    else:
      solution = SolutionAdvanced()
    failure_times = solution.solve(game_id)
    total_failures += failure_times

  print 'Avg Failures: %s' % (float(total_failures) / total_games)

if __name__ == '__main__':
  main(sys.argv)
