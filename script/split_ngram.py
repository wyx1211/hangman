import os

_WORD_FILES = [
  'w2_',
  'w3_',
  'w4_',
  'w5_',
]
_EXTENSION = '.txt'

_BASE_DIR = '../data'

_ADVANCED_WORD_THRESHOLD = 1000

if __name__ == '__main__':
  for word_file in _WORD_FILES:
    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), _BASE_DIR, word_file+_EXTENSION)
    out_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), _BASE_DIR, word_file+str(_ADVANCED_WORD_THRESHOLD)+_EXTENSION)
    f = open(file_path, 'r')
    fout = open(out_file_path, 'w')
    for line in f:
      line = line.strip()
      parts = line.split()
      if int(parts[0]) >= _ADVANCED_WORD_THRESHOLD:
        fout.write(line + '\n')
    f.close()
    fout.close()
